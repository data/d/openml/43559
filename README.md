# OpenML dataset: Moonrise-Moonset--Phases-Timings-(UK-2005-2017)

https://www.openml.org/d/43559

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset contains moonrise, moonset and lunar phase timings for every date from 2005 to 2017 for London, UK*; collected from timeanddate.
Inspiration
This data can be used to study the effects of lunar cycle on any other event of interest. One interesting application for which I made it is to find the correlation of full moon timings with road accidents, to test the Lunar Lunacy effect!  
I'm eager to see what other creative uses it can have?!

*Note: Since only one city can be chosen to retrieve results from the source, hence I have to use London in UK as geographical location. It's noteworthy that moonrise and moonset times will differ for different locations in UK, despite of same timezone due to differences in solar time (in simple words, differences in horizon level). Yet these differences in timings will still be lesser than 30 min (as per observing the contrast between that of eastern and western locations of UK). So if precise timing is not required, then these timings can be used to account for entire UK. And technically, the primary Moon phases occur at a specific moment in time so phase timings will always be same for every location throughout the UK.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43559) of an [OpenML dataset](https://www.openml.org/d/43559). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43559/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43559/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43559/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

